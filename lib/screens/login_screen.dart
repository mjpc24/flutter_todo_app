//Package imports
import 'dart:async';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
//Personal imports created.
import '/utils/api.dart';
import '/utils/functions.dart';
import '/models/user.dart';
import '/providers/user_provider.dart';

//Statefulwidget creations.
class LoginScreen extends StatefulWidget{
    @override
    LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen>{
    Future<User>? _futureLogin;

    final _formKey = GlobalKey<FormState>();
    final _tffEmailController = TextEditingController();
    final _tffPasswordController = TextEditingController();
    //Start of method declarations.
    void login(BuildContext context){
        setState(() {
            _futureLogin = API().login(
                email: _tffEmailController.text,
                password: _tffPasswordController.text
            ).catchError((error){
                showSnackBar(context, error.message);
            });
        });
    }
    //Start of method declarations.
    @override
    Widget build(BuildContext context) {
        
    final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
    final Function setUserId = Provider.of<UserProvider>(context, listen: false).setUserId;

        //BaseClass identifier = subClass().
        //TextFormField is a widget.
        Widget tffEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            //the validator receives the input value of the user
            validator: (value){
                if(value== null || value.isEmpty){
                    return 'The email must be provided.';
                }
                if(EmailValidator.validate(value)== false){
                    return 'A valid Email must be provided';
                }
                return null;
            }
        );
        Widget tffPassword = TextFormField(
            decoration: InputDecoration(labelText:'Password'),
            obscureText: true,
            controller: _tffPasswordController,
            validator: (value){
                bool isVaild = value!= null && value.isNotEmpty;
                return (isVaild) ? null : 'The password must be provided';
            },
        );
        Widget btnSubmit = Container(
            width: double.infinity,
            margin:EdgeInsets.only(top: 8.0),
            child:ElevatedButton(
                child: Text('Login'),onPressed: (){
                    //this is for validation of form using _formKey
                    if (_formKey.currentState!.validate()){
                        login(context);
                    } else {
                        print(_formKey.currentState!.validate());
                        print('The login form is not valid');
                    }
                    // Navigator.pushReplacementNamed(context, '/task-list');
                }
            )
        );

        Widget btnGoToRegister = Container(
            width: double.infinity,
            margin:EdgeInsets.only(top: 8.0),
            child:ElevatedButton(
                child: Text('No Account Yet?'),
                onPressed: (){
                    //navigation for the route
                    Navigator.pushNamed(context, '/register');
                }
            )
        );

        Widget formLogin = Form(
            key: _formKey,
            child: Column(
                children: [
                    tffEmail,
                    tffPassword,
                    btnSubmit,
                    btnGoToRegister
                ],
            ) 
        );

        Widget loginView = FutureBuilder(
            future: _futureLogin,
            builder: (context,snapshot){
                if (_futureLogin == null){
                    return formLogin;
                }else if(snapshot.hasError == true){
                    return formLogin;
                }else if (snapshot.hasData == true)  {

                    Timer(Duration(milliseconds: 1),()async {
                        SharedPreferences prefs = await SharedPreferences.getInstance();
                        User user = snapshot.data as User;

                        setUserId(user.id);
                        setAccessToken(user.accessToken);
                        prefs.setInt('userId',user.id!);
                        prefs.setString('accessToken',user.accessToken!);
                        Navigator.pushReplacementNamed(context, '/task-list');
                    });
                    return Container();
                }
                return Center (
                        child: CircularProgressIndicator()
                );
            }
        );

        return Scaffold(
            appBar: AppBar(title: Text('Task Management Login')),
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.all(16.0),
                child: loginView
            )
        );
    }
}