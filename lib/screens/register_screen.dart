//Package imports
import 'dart:async';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
//Personal imports created.
import '/utils/api.dart';
import '/utils/functions.dart';

class RegisterScreen extends StatefulWidget {
    @override
    RegisterScreenState createState() => RegisterScreenState();
}

class RegisterScreenState extends State<RegisterScreen> {
    Future<bool>? _futureUser;

    //validates the textfields individually only
    final _formKey = GlobalKey<FormState>();
    //to access the current value 
    final _tffEmailController = TextEditingController();
    final _tffPasswordController = TextEditingController();
    final _tffConfirmPasswordController = TextEditingController();
    //Start of method declarations.
    void register(BuildContext context){
        setState(() {
            _futureUser = API().register(
                email: _tffEmailController.text,
                password: _tffPasswordController.text
            ).catchError((error){
                showSnackBar(context, error.message());
            });
        });
    }
    //End of method declarations.
    @override
    Widget build(BuildContext context) {
        Widget tffEmail = TextFormField(
            decoration: InputDecoration(labelText: 'Email'),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            validator: (value){
                if(value== null || value.isEmpty){
                    return 'The email must be provided.';
                }
                if(EmailValidator.validate(value)== false){
                    return 'A valid Email must be provided';
                }
                return null;
            }
        );

    Widget tffPassword = TextFormField(
        decoration: InputDecoration(labelText: 'Password'),
        obscureText: true,
        controller: _tffPasswordController ,
        validator: (value){
            bool isVaild = value!= null && value.isNotEmpty;
            return (isVaild) ? null : 'The password must be provided';
        },
    );

    Widget tffConfirmPassword = TextFormField(
        decoration: InputDecoration(labelText: 'Confirm Password'),
        obscureText: true,
        controller: _tffConfirmPasswordController,
        validator: (passwordConfirmation){
            if (passwordConfirmation == null || passwordConfirmation.isEmpty){
                return 'The password confirmation must be provided';
            } else if(_tffPasswordController.text != passwordConfirmation){
                return 'The Password confirmation does not match with the password above.';
            }
                return null;
        },
    );

    Widget btnRegister = Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 8.0),
        child: ElevatedButton(
            child: Text('Register'),
            onPressed: () {
                if (_formKey.currentState!.validate()){
                    register(context);
                } else {
                    showSnackBar(context, 'Update registration fields to pass all validations');
                }
            }
        )
    );

    Widget btnGoToLogin = Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 8.0),
        child: ElevatedButton(
            child: Text('Go To Login'),
            onPressed: () {
                //counterpart of push to go backward
                Navigator.pop(context);
            }
        )
    );

    Widget formRegister = SingleChildScrollView(
        child: Form(
            key: _formKey,
            child: Column(
                children: [
                    tffEmail,
                    tffPassword,
                    tffConfirmPassword,
                    btnRegister,
                    btnGoToLogin
                ]
            )
        )
    );

    Widget registerView = FutureBuilder(
        future: _futureUser,
        builder:(context,snapshot){
            if (_futureUser == null){
                return formRegister;
            } else if (snapshot.hasData && snapshot.data == true) {
                Timer(Duration(seconds: 3),(){
                        
                    Navigator.pushReplacementNamed(context, '/');        
                });
                return Column(
                    children: [
                        Text('registration successful, You will b redirected shortly back to the log in page.'),
                            
                    ],
                );
            } else {
                return Center(
                    child:CircularProgressIndicator()
                );
            }
        }
    );

    return Scaffold(
        appBar: AppBar(title: Text('Todo Account Registration')),
        body: Container(
            width: double.infinity,
            padding: EdgeInsets.all(16.0),
            child: registerView
            )
        );
    }
}