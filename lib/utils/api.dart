import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '/models/user.dart';
import '/models/task.dart';

class API {
    // For requests from an Android virtual device, the URL should be 10.0.2.2 instead of 127.0.0.1.
    final String? _url = dotenv.env['API_URL'];
    final String? _accessToken;

    API([this._accessToken]);

    Future<bool> register({
        required String email,
        required String password
    }) async {
        try {
            final response = await http.post(
                Uri.parse('$_url/users'),
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8'
                },
                body: jsonEncode({
                    'email': email,
                    'password': password
                })
            );

            if (response.statusCode == 200) {
                return jsonDecode(response.body);
            } else {
                throw Exception('User registration procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<User> login({ 
        required String email, 
        required String password 
    }) async {
        try {
            final response = await http.post(
                Uri.parse('$_url/users/login'),
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                },
                body: jsonEncode({
                    'email': email,
                    'password': password
                })
            );

            if (response.statusCode == 200) {
                var data = jsonDecode(response.body);

                if (data['error'] != null) {
                    throw Exception(data['error']);
                } else {
                    return User.fromJson(data);
                }
            } else {
                throw Exception('User authentication procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<bool> addTask({ 
    required String description 
    }) async {
        try {
            final response = await http.post(
                Uri.parse('$_url/tasks'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                },
                body: jsonEncode({
                    'description': description
                })
            );

            if (response.statusCode == 200) {
                return jsonDecode(response.body);
            } else {
                throw Exception('Task creation procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<List<Task>> getTasks() async {
        try {
            final response = await http.get(
                Uri.parse('$_url/tasks'),
                headers: {
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                }
            );

            if (response.statusCode == 200) {
                return (jsonDecode(response.body) as List).map((task) => Task.fromJson(task)).toList();
            } else {
                throw Exception('Tasks cannot be retrieved from the server.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<bool> toggleTaskStatus({ 
        required int id, 
        required int isDone 
    }) async {
        try {
            final response = await http.put(
                Uri.parse('$_url/tasks/toggle-status'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                },
                body: jsonEncode({
                    'id': id,
                    'isDone': isDone
                })
            );

            if (response.statusCode == 200) {
                return jsonDecode(response.body);
            } else {
                throw Exception('Task update procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }


}