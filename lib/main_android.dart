//Package imports
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
//Personal Imports created
import 'providers/user_provider.dart';
import '/screens/login_screen.dart';
import '/screens/register_screen.dart';
import '/screens/task_list_screen.dart';

Future<void> main() async {
    //initial checks for user access token from sharedPreferences.
    //Determine initial route of app depending on existence of users access token
    //ensures all widgets are ready
    WidgetsFlutterBinding.ensureInitialized();
    await DotEnv().load(fileName: 'assets/env/.env_android');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? accessToken = prefs.getString('accessToken');
    String initialRoute = (accessToken != null) ? '/task-list' : '/';

    runApp(App(accessToken, initialRoute));
}

class App extends StatelessWidget{
    final String? _accessToken;
    final String _initialRoute;
    App(this._accessToken,this._initialRoute);

    @override
    Widget build(BuildContext context) {
            return ChangeNotifierProvider(
                create: (BuildContext context) => UserProvider(_accessToken),
                child:MaterialApp(
                theme: ThemeData(
                    primaryColor: Color.fromRGBO(255, 212, 71, 1),
                    //this will apply to all elevated button across the APP
                    elevatedButtonTheme: ElevatedButtonThemeData(
                        style: ElevatedButton.styleFrom(
                            primary: Color.fromRGBO(255, 212, 71, 1), //button color
                            onPrimary: Colors.black //text color
                        )
                    )
                ),
                initialRoute: _initialRoute,
                routes: {
                    '/': (context) => LoginScreen(),
                    '/register': (context) => RegisterScreen(),
                    '/task-list': (context) => TaskListScreen(),
                }
            )
        );
    }
}


