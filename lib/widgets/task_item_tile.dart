//Package imports
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//Personal Imports created.
import '/models/task.dart';
import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';


class TaskItemTile extends StatefulWidget {
    final Task _task;

    TaskItemTile(this._task);

    @override
    TaskItemTileState createState() => TaskItemTileState();
}

class TaskItemTileState extends State<TaskItemTile> {  
    late bool _isDone;

    @override
    void initState(){
        super.initState();
        setState(() {
            _isDone =widget._task.isDone == 1;
        });
    }

    @override
    Widget build(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context, listen: false).accessToken;

        return CheckboxListTile(
            title: Text(widget._task.description),
            value: _isDone,
            //leading checkbox on the left side
            controlAffinity: ListTileControlAffinity.leading,
            //secondary on the right side
            secondary: IconButton(
                icon: Icon(Icons.info),
                onPressed: (){},
            ),
            onChanged: (bool? value) {
                API(accessToken).toggleTaskStatus(
                    id: widget._task.id,
                    isDone: (_isDone == true) ? 1 : 0
                ).then((value){
                    if (value == true){
                        setState (()=>_isDone = !_isDone);
                    }
                }).catchError((error){
                    showSnackBar(context,error.message);
                });
            }
        );
    }
}