//import freezed annotation
import 'package:freezed_annotation/freezed_annotation.dart';
part 'task.freezed.dart';
part 'task.g.dart';

//The freezed annotation tells the buld_runner to create a freexed class named _$User
@freezed 
class Task with _$Task {
    const factory Task ({
        required int id,
        required int userId,
        required String description,
        String? imageLocation,
        required int isDone
    }) = _Task;

    factory Task.fromJson(Map<String,dynamic> json) => _$TaskFromJson(json);
}